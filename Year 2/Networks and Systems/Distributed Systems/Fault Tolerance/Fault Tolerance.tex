\documentclass{article}[18pt]
\input{../../../../format}
\lhead{Networks and Systems - Distributed Systems}


\begin{document}
\begin{center}
\underline{\huge Fault Tolerance}
\end{center}
\begin{definition}[Fault tolerance]
Ability of a system to continue error-free operation even in the presence of unexpected fault
\end{definition}
\section{Approaches based on redundancy}
\begin{itemize}
	\item Apply duplication to increase system reliability
	\item System architecture approach
	\begin{itemize}
		\item Incorporate Active or Passive replication
		\item Design server configuration and number of replicated servers
		\item Could be expensive due to requiring extra hardware
	\end{itemize}
	\item Operational approach
	\begin{itemize}
		\item Replicate system operations to offer fault tolerance
		\begin{itemize}
			\item Time redundancy
			\item Component redundancy
			\item Information redundancy
			\item Communication redundancy
		\end{itemize}
	\end{itemize}
\end{itemize}
\subsection{Time redundancy}
\begin{itemize}
	\item Perform the same operation multiple times
	\item No fault if getting the same result each time
	\item Detect temporary faults but not permanent ones
	\item Impact system performance
\end{itemize}
\begin{center}
	\includegraphics[scale=0.7]{"Time Redundancy"}
\end{center}
\subsection{Component redundancy}
Replicate component and compare outputs:
\begin{itemize}
	\item Introduce two or more independent running components which provide the same functionalities
	\item Impose little or no performance impact
\end{itemize}
N-Version Programming (NVP):
\begin{itemize}
	\item Design diversity - implementing multiple versions of the program
	\item Tolerate hardware and software faults, but not correlated faults
\end{itemize}
\begin{center}
	\includegraphics[scale=0.7]{"Component Redundancy"}
\end{center}
\subsection{Information Redundancy}
Encode outputs with error detection or correcting code\\
Advantage:
\begin{itemize}
	\item Less hardware is required than replicating module
	\item Support fault detection
\end{itemize}
Drawback
\begin{itemize}
	\item Added complexity in design
	\item Fault recovery capability may be limited
\end{itemize}
\section{Communication Failures}
Client is unable to locate server
\begin{itemize}
	\item Use an exception handler (programming language dependent)
	\item Check out available/update servers from a directory service
\end{itemize}
Client request to server is lost:
\begin{itemize}
	\item Apply timeout to await server reply then re-send
	\item If multiple requests appear to get lost assume "can't locate server" error
\end{itemize}
Server crashes after receiving client request
\begin{itemize}
	\item Server may stop before or after returning the info, or before ACK
	\item Store user request in the FE
	\item Rebuild or use alternate server to retry request
	\item Give up and report failure
\end{itemize}
Server reply to client is lost
\begin{itemize}
	\item Apply timeout to await server reply
\end{itemize}
\section{General Workflow towards fault tolerance}
\begin{itemize}
	\item Error (or fault) detection and diagnosis
	\item Failure isolation - system must be able to isolate the failure to the offending component
	\item Error containment - confine the effects of hardware or software faults to the region where they occur
	\item Recovery - move the system to a state that does not contain the error
\end{itemize}
\section{Backwards recovery}
\begin{definition}[Backward Recovery]
Move the system back to a failure free state
\end{definition}
Checkpointing
\begin{itemize}
	\item Each DS component periodically saves its state, which contains sufficient information to restart component execution
\end{itemize}
\begin{definition}[Global Checkpoint]
To support state saving, a consistent global checkpoint can be set up, which comprises a set of N local checkpoints, one from each DS component, forming a consistent system state
\end{definition}
\begin{itemize}
	\item Component execution can be restarted upon failure by following the saved checkpoints
	\item The most recent consistent checkpoint is called the recovery line
\end{itemize}
\subsection{Types of checkpointing}
\begin{definition}[Uncoordinated checkpointing]
Each process takes its checkpoints independently
\end{definition}
\begin{definition}[Coordinated checkpointing]
Processes coordinate process checkpoints in order to save a global consistent state
\end{definition}
\begin{definition}[Communication induced checkpointing]
Force each process to take checkpoints based in information piggybacked from the application it receives from other processes	
\end{definition}
\subsection{Domino effect}
\begin{definition}[Domino effect]
Cascaded rollback which causes the system to roll back too far in the computation
\end{definition}
As a consistent state is needed, if the checkpoints are not synchronised (as in uncoordinated checkpointing) there might need to be a huge rollback in order to have a state they can roll back to
\newpage
\section{Forward Recovery}
\begin{definition}[Forward recovery]
Find a new state from which the system can continue operation
\end{definition}
Methods to avoid stopping the system:
\begin{definition}[Self checking components]
Switch from a failed to a non failed component executing the same task code
\end{definition}
\begin{definition}[Fault masking]
Error compensation is continuously applied
\end{definition}
\begin{definition}[Error compensation/resilience]
Using an algorithm that has redundancy such as multiple processes sending redundant data
\end{definition}
\begin{definition}[Data Prediction]
Simulate application response
\end{definition}
\section{Comparison between forward and backward recovery}
\begin{minipage}[t]{0.4\textwidth}
\begin{center}
	\underline{Backward Recovery}
\end{center}
\begin{itemize}
	\item Require no knowledge about the error
	\item Only need to mainatin some prior error free state
	\item Application independent
	\item Limitations: resource/time consumption, domino effect
\end{itemize}
\end{minipage}
\begin{minipage}{0.1\textwidth}
	\vline
\end{minipage}
\begin{minipage}[t]{0.4\textwidth}
	\begin{center}
	\underline{Forward Recovery}
	\end{center}
\begin{itemize}
	\item Efficient in terms of time and storage space
	\item Require knowledge of error
	\item Application dependent
	\item Use when significant system delay is not acceptable
\end{itemize}
\end{minipage}
\section{Measurement of system quality}
\begin{definition}[Reliability]
The extent to which a system yields expected results on repeated trials
\end{definition}
Reliability is measured by the mean time between failures (MTBF)
\begin{definition}[Availability]
The fraction of the time the system yields expected results, this is reduced by downtime, repair and preventative maintenance
\end{definition}
Availability is measured as follows
$$A=\dfrac{MTBF}{MTBF+MTTR}$$
where MTTR is mean time to repair
\section{Goals}
{\renewcommand{\arraystretch}{2}
\newcolumntype{L}{>{\raggedright\arraybackslash}X}%
\begin{tabularx}{\textwidth}{|L|L|L|L|L|}
\hline
System Type&Issues&Goal&Examples&Techniques\\
\hline
Long life systems&Difficult or expensive to repair&Maximise MTBF&Satellites&Dynamic Redundancy\\
\hline
Reliable real time systems&Error or delay catastrophic&Fault masking capability&Air bags&Triple module redundancy\\
\hline
High availability systems&Downtime very costly&High availability&Stock exchange&No single point of failure, self checking pairs, fault isolation\\
\hline
High integrity systems&Data corruption very costly&High data integrity&Banking database&Checkpointing, time redundancy, error detection and correction, redundant disks\\
\hline
Mainstream Low-Cost Systems&Reasonable level of failures acceptable&Meet failure rate expectations at low cost&Personal computers&Often none\\
\hline
\end{tabularx}}


\end{document}
